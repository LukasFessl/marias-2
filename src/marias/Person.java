/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marias;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Lukas
 */
public class Person implements Serializable{
    
    private int id;
    private String name;
    private ArrayList<Character> seatMap = new ArrayList<Character>(Arrays.asList('a', 'b', 'c', 'd','e', 'f','g','h'));
    private int[][] games = new int[3][2];
    
    public Person(){}
    
    public Person(String name) {
        this.name = name;
        games[0] = new int[2];
        games[1] = new int[2];
        games[2] = new int[2];
    }
    
    public Person(Person person) {
        this.id = person.getId();
        this.name = person.getName();
        games[0] = new int[2];
        games[1] = new int[2];
        games[2] = new int[2];
    }
    
    public void setGame(int index, int[] game) {
        this.games[index] = game;
    }
    
    public int[] getGame(int index) {
        return this.games[index];
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
           
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
        
    public void setSeat(int gameIndex, int seat) {
        this.games[gameIndex][1] = seat;
    }
    
    public int getSeat(int index) {
        return games[index][1];
    }
    
    public void setTable(int gameIndex, int table) {
        this.games[gameIndex][0] = table;
    }
    
    public int getTable(int index) {
        return games[index][0];
    }
    
    public String getTableAndSeat(int index) {
        return games[index][0]+" / "+this.getSeatFromMap(games[index][1]);
    }
    
    private String getSeatFromMap(int index) {
        return ""+this.seatMap.get(index);
    }
    
    public String toString() {
        String s = (this.id+1)+". "+this.name;  
        return s;
    }
    
    public int getNumber() {
        return this.id+1; 
    }
}
