/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marias;

import java.io.File;
import java.io.IOException;

import jxl.format.Alignment;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Lukas
 */
public class ExcelExport {
    
    private String filename;
    private String turnamentName;
    
    public ExcelExport(String filename, String turnamentName)
    {
        this.filename = filename;
        this.turnamentName = turnamentName;
    }
    
    public void export(Turnament turnament) throws IOException, WriteException
    {
        WritableWorkbook workbook = Workbook.createWorkbook(new File(this.filename));
        WritableSheet wsheet = workbook.createSheet(this.turnamentName, 0);
        wsheet.addCell(new Label(0, 0, "ID")); 
        wsheet.addCell(new Label(1, 0, "Jméno")); 
        wsheet.addCell(new Label(2, 0, "Výhra")); 
        
        WritableCellFormat cellFormat = new WritableCellFormat();
        cellFormat.setAlignment(Alignment.LEFT);

        for(int i = 0; i < turnament.getPeople().size(); i++) {
            Person p = turnament.getPerson(i);
 
            wsheet.addCell(new Number(0, i+1, p.getId(), cellFormat));
            wsheet.addCell(new Label(1, i+1, p.getName())); 
            
        }
        
        
        
         Number number = new Number(3, 4, 3.1459);
        workbook.write();
        workbook.close();
        
               
    }
    
}
