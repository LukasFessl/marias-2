/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marias;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Lukas
 */
public class Turnament implements Serializable{

    private String name;
    private int tablesCount;
    private int playersCount;
    private ArrayList<Person> people = new ArrayList<Person>();
    private ArrayList[] games = new ArrayList[3];
    
    
    public ArrayList getGame(int index) {
        return games[index];
    }
    
    public ArrayList[] getGames() {
        return games;
    }
    
    public Person getPerson(int index) {
        return people.get(index);
    }
    
    public void addPerson(Person person) {
        this.people.add(person);
    }
    
    public void addPerson(int index, Person person) {
        this.people.add(index, person);
    }
    
    public void addPeople(ArrayList<Person> people) {
        this.people = new ArrayList<Person>(people);
    }
    
    public ArrayList<Person> getPeople() {
        return this.people;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getTableCount() {
        return this.tablesCount;
    }
    
    public void setTableCount(int count) {
        this.tablesCount = count;
    }
    
    public void printGame() {
        System.out.println();
        ArrayList<Person> temp;
        int c = 0;
        for(int i=0; i<3; i++){
            temp = games[i];
            for(Person p : temp) {
                if(c == p.getTable(i))
                    System.out.println();
                System.out.print(p.getName()+" "+p.getTable(i)+"/"+p.getSeat(i)+" "); 
                c=1;
            }
            c = 0;
            System.out.println();
            System.out.println();
        }
    }
    
    
    public void generateGame() {
        games[0] = new ArrayList<Person>();
        games[1] = new ArrayList<Person>();
        games[2] = new ArrayList<Person>();
        ArrayList<Person> peopleTemp = new ArrayList<Person>(people);
        Random rand = new Random();

        ArrayList<Integer> seats = new ArrayList<Integer>();
        for(int i = 0; i < 10;i++) {
            seats.add(i);
        }

        for(int i = 0; i<3; i ++) {
            int tableCount = 1;
            for (int j = 0; j< people.size(); j++) {
                int r = rand.nextInt(peopleTemp.size());
                Person p = peopleTemp.get(r);
                p.setTable(i, tableCount);
                p.setSeat(i, 1);
                games[i].add(j, p);
                peopleTemp.remove(r); 
                
                tableCount++;
                if(tableCount > this.tablesCount)
                    tableCount = 1;   
            }
            peopleTemp = new ArrayList<Person>(games[i]);
        }
        
        generateTableSeat();
        
    }
    
    
    private void generateTableSeat()
    {
        ArrayList<Integer> seats = new ArrayList<Integer>();
        for(int i = 0; i < playersCount; i++)
            seats.add(i);
        
        ArrayList[] tables = new ArrayList[tablesCount];
        
        for(int i = 0; i < tablesCount; i++)
            tables[i] = new ArrayList<Integer>(seats);
        
        Random rand = new Random();
        for(int j = 0; j < 3; j++) {    //game
            for(int i = 0; i < people.size(); i++) {
                int r = rand.nextInt(tables[people.get(i).getTable(j)-1].size());
                people.get(i).setSeat(j, (int) tables[people.get(i).getTable(j)-1].get(r));
                tables[people.get(i).getTable(j)-1].remove(r);
            }
            
            tables = new ArrayList[tablesCount];
            for(int i = 0; i < tablesCount; i++)
                tables[i] = new ArrayList<Integer>(seats);
        }

    }
    
    
    
    
    
    
    public String toString() {
        return this.name;
    }

    void setPlayesrsCount(int players) {
        this.playersCount = players;
    }
}
